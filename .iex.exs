fake_work = fn num_messages ->
  Enum.map(1..num_messages, fn _ ->
    sld = Enum.random([
      "google",
      "youtube",
      "en.wikipedia",
      "myanimelist",
      "arxiv",
      "website",
      "example",
      "elixir",
      "hex",
      "potions",
      "free"
    ])
    tld = Enum.random([".com", ".org", ".net", ".io", ".pro"])
    host = sld <> tld
    work = {Enum, :random, [1..1000]}
    %{host: host, work: work}
  end)
end

