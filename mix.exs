defmodule RequestProcessor.MixProject do
  use Mix.Project

  def project do
    [
      app: :request_processor,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {RequestProcessor.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:broadway, "~> 1.0"},       # data pipelines
      {:gen_stage, "~> 1.1"},      # data pipeline ingestion
      {:phoenix_pubsub, "~> 2.1"}, # handle_continue helper
    ]
  end
end
