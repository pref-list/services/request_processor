# RequestProcessor

Limits number of requests per `host`. For example, allows
scraping a number of different websites in parallel, while
always making sure that only one request is made for each
different host to prevent scraper from being banned.

## API

Pass a list of tasks to `RequestProcessor.RequestProducer.place_requests/1`.
Task is a map with mandatory fields `host` and `work`.

`host` needs to be a term, and work is model-function-arguments tuple:
`{Module, function, args}`. For example, `{Enum, :sum, [[1,2,3]]}`.

For example:
```elixir
%{host: "example.com", work: {Enum, :random, [1..1000]}}
```

Function `work` would be given the overall task map when it is executed.

`.iex.exs` provides convenience function for generating many fake tasks:
```elixir
RequestProcessor.place_requests(fake_work.(100000))
```

Note that with high load, some jobs might be dropped. If you wish to be able
to add them using `handle_demand` logic, you might want to use
`RequestProcessor.subscribe_handle_demand/0` to subscribe to corresponding
Phoenix.PubSub channel.

```elixir
defmodule PrefEngine.PageLinkGenerator do
  use GenServer, start: {__MODULE__, :start_link, []}, restart: :permanent

  def start_link(), do:
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)

  def init(:ok) do
    RequestProcessor.subscribe_handle_demand()
    {:ok, []}
  end
end
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `request_processor` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:request_processor, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/request_processor>.

