defmodule RequestProcessor do
  alias RequestProcessor.{HostRunner, HostPipeline, HostRegistry}

  ## Public API
  def place_requests(requests), do:
    RequestProcessor.RequestProducer.place_requests(requests)

  def subscribe_handle_demand(), do:
    Phoenix.PubSub.subscribe RequestProcessor.PubSub, "host_demand"

  ## Useful helpers
  def host_pipeline_online?(host) do
    with {:ok, _pid} <- start_host(host) do
      true
    else
      {:error, _error} -> false
    end
  end

  def start_host(host) do
    pid = get_host(host)
    if pid do
      {:ok, pid}
    else
      DynamicSupervisor.start_child(HostRunner, {HostPipeline, host})
    end
  end
  
  def get_host(host) do
    match_all = {:"$1", :"$2", :"$3"}
    guards = [{:"==", :"$1", host}]
    map_result = [:"$2"]

    Registry.select(HostRegistry, [{match_all, guards, map_result}])
    |> List.first 
  end

  def running_imports() do
    match_all = {:"$1", :"$2", :"$3"}
    guards = [{:"==", :"$3", "import"}]
    map_result = [%{id: :"$1", pid: :"$2", type: :"$3"}]

    Registry.select(HostRegistry, [{match_all, guards, map_result}])
  end

  # Just for ease of debug
  def list_all() do
    match_all = {:"$1", :"$2", :"$3"}
    guards = []
    map_result = [%{id: :"$1", pid: :"$2", type: :"$3"}]

    Registry.select(HostRegistry, [{match_all, guards, map_result}])
  end
end
