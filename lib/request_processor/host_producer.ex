defmodule RequestProcessor.HostProducer do
  use GenStage
  require Logger

  def init(host) do
    Logger.info("#{host} HostProducer init")
    {:producer, host}
  end

  def handle_demand(demand, host) do
    Phoenix.PubSub.broadcast(RequestProcessor.PubSub,
      "host_demand",
      {:host_demand, %{demand: demand, host: host}}
    )
    events = []
    {:noreply, events, host}
  end

  def place_requests(pipeline, requests) when is_list(requests) do
    pipeline
    |> Broadway.producer_names()
    |> List.first()
    |> GenStage.cast({:requests, requests})
  end

  def handle_cast({:requests, requests}, host) do
    {:noreply, requests, host}
  end
end
