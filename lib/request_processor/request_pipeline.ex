defmodule RequestProcessor.RequestPipeline do
  use Broadway
  require Logger
  alias RequestProcessor.{RequestProducer, RequestPipeline}
  alias RequestProcessor.{HostProducer, HostPipeline}

  def start_link(_args) do
    options = [
      name: RequestPipeline,
      producer: [
        module: {RequestProducer, []},
        transformer: {RequestPipeline, :transform, []}
      ],
      processors: [
        default: [max_demand: 1, concurrency: 1]
      ],
      batchers: [
        default: [concurrency: 1, batch_size: 1, batch_timeout: 10]
      ]
    ]

    Broadway.start_link(__MODULE__, options)
  end

  def transform(event, _options) do
    %Broadway.Message{
      data: event,
      acknowledger: {__MODULE__, :pages, []}
    }
  end

  def ack(:pages, _successful, _failed) do
    :ok
  end

  def handle_message(_processor, message, _context) do
    if RequestProcessor.host_pipeline_online?(message.data.host) do
      Broadway.Message.put_batch_key(message, message.data.host)
    else
      Broadway.Message.failed(message, "host error")
    end
  end

  def handle_batch(_batcher, [message], _batch_info, _context) do
    #Logger.info("Batch Processor received #{inspect(message.data)}")
    HostProducer.place_requests(HostPipeline.via(message.data.host), [message.data])
    [message]
  end
end
