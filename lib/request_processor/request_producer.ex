defmodule RequestProcessor.RequestProducer do
  use GenStage
  require Logger

  def init(initial_state) do
    Logger.info("RequestProducer init")
    {:producer, initial_state}
  end

  def handle_demand(demand, state) do
    #Logger.info("RequestProducer received demand for #{demand} requests")
    events = []
    {:noreply, events, state}
  end

  def place_requests(requests) when is_list(requests) do
    RequestProcessor.RequestPipeline
    |> Broadway.producer_names()
    |> List.first()
    |> GenStage.cast({:requests, requests})
  end

  def handle_cast({:requests, requests}, state) do
    {:noreply, requests, state}
  end
end
