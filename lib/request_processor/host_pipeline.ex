defmodule RequestProcessor.HostPipeline do
  use Broadway
  require Logger
  alias RequestProcessor.{HostRegistry, HostProducer, HostPipeline}

  @host_throttle 2000

  def via(key, value) do
    {:via, Registry, {HostRegistry, key, value}}
  end
  
  def via(key) do
    {:via, Registry, {HostRegistry, key}}
  end

  def process_name({:via, Registry, {HostRegistry, host}}, base_name) do
    via({host, base_name})
  end

  def start_link(host) do
    options = [
      name: via(host),
      producer: [
        module: {HostProducer, host},
        transformer: {HostPipeline, :transform, []}
      ],
      processors: [
        default: [max_demand: 1, concurrency: 1]
      ],
      batchers: [
        default: [concurrency: 1, batch_size: 1, batch_timeout: 10]
      ]
    ]

    Broadway.start_link(__MODULE__, options)
  end

  def transform(event, _options) do
    %Broadway.Message{
      data: event,
      acknowledger: {__MODULE__, :pages, []}
    }
  end

  def ack(:pages, _successful, _failed) do
    :ok
  end

  def handle_message(_processor, message, _context) do
    Broadway.Message.put_batch_key(message, message.data)
  end

  def handle_batch(_batcher, [message], _batch_info, _context) do
    do_work(message.data)
    #message.data.work.(message.data)
    Process.sleep(@host_throttle)
    [message]
  end

  defp do_work(%{host: host, work: {module, fun, args}}) do
    Logger.info("Processing work for #{host}: #{inspect(apply(module, fun, args))}")
  end
end
